# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-05-10 17:07-0300\n"
"PO-Revision-Date: 2016-05-10 17:07-0300\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../controllers/gov_user_plugin_controller.rb:142
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:87
msgid "Select a Governmental Sphere"
msgstr ""

#: ../controllers/gov_user_plugin_controller.rb:147
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:98
msgid "Select a Governmental Power"
msgstr ""

#: ../controllers/gov_user_plugin_controller.rb:152
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:109
msgid "Select a Juridical Nature"
msgstr ""

#: ../controllers/gov_user_plugin_controller.rb:186
#: ../controllers/gov_user_plugin_myprofile_controller.rb:30
msgid "Could not find Governmental Power or Governmental Sphere"
msgstr ""

#: ../controllers/gov_user_plugin_controller.rb:232
msgid "Institution successful created!"
msgstr ""

#: ../controllers/gov_user_plugin_controller.rb:237
msgid "Institution could not be created!"
msgstr ""

#: ../lib/ext/organization_rating.rb:16
msgid "institution not found"
msgstr ""

#: ../lib/ext/organization_rating.rb:23
msgid "Report values cannot be saved without an institution"
msgstr ""

#: ../lib/ext/search_controller.rb:6
msgid "Communities Search"
msgstr ""

#: ../lib/ext/search_controller.rb:18
msgid "Institutions Search"
msgstr ""

#: ../lib/gov_user_plugin.rb:21
msgid "Add features related to Brazilian government."
msgstr ""

#: ../lib/gov_user_plugin.rb:134 ../lib/gov_user_plugin.rb:166
msgid "Create Institution"
msgstr ""

#: ../lib/gov_user_plugin.rb:306
msgid "Institution Info"
msgstr ""

#: ../lib/gov_user_plugin.rb:331
#: ../views/organization_ratings_container_extra_fields_show_institution.html.erb:4
#: ../views/organization_ratings_task_extra_fields_show_institution.html.erb:4
msgid "Institution"
msgstr ""

#: ../lib/institution.rb:66
msgid "invalid, only public and private institutions are allowed."
msgstr ""

#: ../lib/institution.rb:78 ../lib/institution.rb:89 ../lib/institution.rb:105
msgid "can't be blank"
msgstr ""

#: ../lib/institution.rb:92
msgid "invalid state"
msgstr ""

#: ../lib/institution_modal_helper.rb:9
msgid "Create new institution"
msgstr ""

#: ../lib/institution_modal_helper.rb:55
msgid "New Institution"
msgstr ""

#: ../lib/institutions_block.rb:4 ../views/person_editor_extras.html.erb:2
msgid "Institutions"
msgstr ""

#: ../lib/institutions_block.rb:12
msgid "{#} institution"
msgid_plural "{#} institutions"
msgstr[0] ""
msgstr[1] ""

#: ../lib/institutions_block.rb:16
msgid "This block displays the institutions in which the user is a member."
msgstr ""

#: ../lib/institutions_block.rb:24 ../lib/institutions_block.rb:30
msgid "institutions|View all"
msgstr ""

#: ../test/unit/gov_user_person_test.rb:49
#: ../test/unit/gov_user_person_test.rb:55
msgid "Name Should begin with a capital letter and no special characters"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:5
msgid ""
"Note that the creation of institutions in this environment is restricted. Your"
" request to create this new community will be sent to %{environment} administr"
"ators and will be approved or rejected according to their methods and criteria"
"."
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:11
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:23
msgid "The highlighted fields are mandatory"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:18
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:11
msgid "\"Can`t create new Institution: #{flash[:errors].length} errors\""
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:34
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:34
msgid "Public Institution"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:38
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:37
msgid "Private Institution"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:48
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:45
msgid "Corporate Name"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:53
msgid "Institution name already exists"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:57
#: ../views/gov_user_plugin/_institution.html.erb:105
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:48
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:76
msgid "Fantasy name"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:65
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:54
msgid "Country"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:69
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:54
msgid "Select a country"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:74
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:58
msgid "State"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:78
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:58
msgid "Select a state"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:83
msgid "City"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:95
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:68
msgid "CNPJ"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:104
#: ../views/gov_user_plugin/_institution.html.erb:106
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:75
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:77
msgid "Acronym"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:114
msgid "Governmental Sphere"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:125
msgid "Governmental Power"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:136
msgid "Juridical Nature"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:150
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:121
msgid "SISP?"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:155
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:126
#: ../views/profile/_institution_tab.html.erb:19
msgid "Yes"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:160
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:130
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:133
#: ../views/profile/_institution_tab.html.erb:19
msgid "No"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:170
#: ../views/gov_user_plugin/_institution.html.erb:173
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:143
msgid "Save"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:171
#: ../views/gov_user_plugin/_institution.html.erb:174
#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:144
msgid "Cancel"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:175
msgid "Could not send the form data to the server"
msgstr ""

#: ../views/gov_user_plugin/_institution.html.erb:183
msgid "Creating institution"
msgstr ""

#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:1
msgid "Edit Institution"
msgstr ""

#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:5
msgid ""
"Note that the creation of communities in this environment is restricted. Your "
"request to create this new community will be sent to %{environment} administra"
"tors and will be approved or rejected according to their methods and criteria."
msgstr ""

#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:86
#: ../views/profile/_institution_tab.html.erb:17
msgid "Governmental Sphere:"
msgstr ""

#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:97
#: ../views/profile/_institution_tab.html.erb:16
msgid "Governmental Power:"
msgstr ""

#: ../views/gov_user_plugin_myprofile/edit_institution.html.erb:108
#: ../views/profile/_institution_tab.html.erb:18
msgid "Juridical Nature:"
msgstr ""

#: ../views/incomplete_registration.html.erb:3
msgid "Complete Profile"
msgstr ""

#: ../views/incomplete_registration.html.erb:6
msgid "Complete your profile"
msgstr ""

#: ../views/incomplete_registration.html.erb:7
msgid "Hide"
msgstr ""

#: ../views/organization_ratings_task_extra_fields_show_institution.html.erb:9
msgid ""
"This institution already has an accepted rating. Accepting it will automatical"
"ly update the saved value."
msgstr ""

#: ../views/person_editor_extras.html.erb:6
msgid "Add institution"
msgstr ""

#: ../views/person_editor_extras.html.erb:13
msgid "No institution found"
msgstr ""

#: ../views/person_editor_extras.html.erb:32
msgid "Should begin with a capital letter and no special characters"
msgstr ""

#: ../views/person_editor_extras.html.erb:33
msgid "Email should have the following format: name@host.br"
msgstr ""

#: ../views/person_editor_extras.html.erb:34
msgid "Site should have a valid format: http://name.hosts"
msgstr ""

#: ../views/person_editor_extras.html.erb:35
msgid "If you work in a public agency use your government e-Mail"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:3
msgid "Institution Information"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:6
msgid "Type:"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:7
msgid "CNPJ:"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:8
msgid "Last modification:"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:9
msgid "Country:"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:10
msgid "State:"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:11
msgid "City:"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:13
msgid "Fantasy Name:"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:15
msgid "Acronym:"
msgstr ""

#: ../views/profile/_institution_tab.html.erb:19
msgid "SISP:"
msgstr ""

#: ../views/ratings_extra_field.html.erb:2
msgid "Organization name or Enterprise name"
msgstr ""

#: ../views/ratings_extra_field.html.erb:7
msgid "No organization or company found"
msgstr ""

#: ../views/ratings_extra_field.html.erb:9
msgid "Add"
msgstr ""

#: ../views/search/institutions.html.erb:3
msgid "Type words about the %s you're looking for"
msgstr ""
